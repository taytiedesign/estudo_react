const INITIAL_STATE = [];
function favorites(state = INITIAL_STATE, action) {
    switch (action.type) {
        case "ADD_FAVORITE":
            return [
                ...state,
                {
                    id: Math.random(),
                    name: "Nome do repositorio",
                    description: "descrição do repositorio",
                    url: "http://www.uol.com.br",
                },
            ];
        default:
            return state;
    }
}

export default favorites;
