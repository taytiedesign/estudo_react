import React, { Fragment, Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as FavoritesActions from "../../store/actions/favorites";

class Main extends Component {
    state = {
        respositoryInput: "",
    };
    handleAddRepository = (event) => {
        event.preventDefault();
    };
    render() {
        return (
            <Fragment>
                <form onSubmit={this.handleAddRepository}>
                    <input
                        type="text"
                        placeholder="usuario/repositorio"
                        value={this.state.respositoryInput}
                        onChange={(e) =>
                            this.setState({ respositoryInput: e.target.value })
                        }
                    />
                    <button type="submit">Adicionar Repositorio</button>
                </form>
                <ul>
                    <li>
                        <p>
                            <strong>Facebook/react</strong> ( descrição do
                            respositorio )
                        </p>
                    </li>
                </ul>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    favorites: state.favorites,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(FavoritesActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Main);
